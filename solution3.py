# Python Glasgow Dojo
# Solution 3
# Changed for python3.6
from collections import Counter, defaultdict, namedtuple
from datetime import date

Task = namedtuple('Task', ['date','type','assignee'])

TASKS = [
        Task( date(2017,10,25), 'driving', None),
        Task( date(2017,10,25), 'shopping', None),
        Task( date(2017,10,25), 'cooking', None),
        ]

SKILLS = {
        'driving': set(['Roger','Gordon','Erfan']),
        'cooking': set(['Yuri','Erfan']),
        'shopping': set(['Roger']),
        }

VACATIONS = defaultdict(set, {
    date(2017,10,25) : set(),
    date(2017,10,26) : set(['Roger', 'Gordon']),
    })

"""
We now present a solution for the case of skills, vactions
and volunteers.

For volunteers, we assume some fo the input tasks have already
been assigned.
"""

def assignTasks(tasks):
    allPeople = set(['Erfan', 'Gordon', 'Roger', 'Yuri'])

    taskAssigner = TaskAssigner(allPeople, SKILLS, VACATIONS)
    assignedTasks = taskAssigner.assignTasks(tasks)

    return assignedTasks

class PersonSelector(object):

    def __init__(self, allPeople):
        self.counter = Counter({x:0 for x in allPeople})

    def recordWork(self, assignees):
        self.counter.update(assignees)

    def selectFrom(self, eligiblePersonSet):
        for person, _cnt in reversed(self.counter.most_common()):
            if person in eligiblePersonSet:
                self.counter[person] += 1
                return person

class TaskAssigner(object):

    def __init__(self, allPeople, skills, vacations):
        self.ps = PersonSelector(allPeople)
        self.skills, self.vacations = skills, vacations

    def _availablePeople(self, task):
        return self.skills[task.type] - self.vacations[task.date]

    def assignTasks(self, tasks):
        assignedTasks = [t for t in tasks if t.assignee]
        unassignedTasks = [t for t in tasks if not t.assignee]
        self.ps.recordWork([t.assignee for t in assignedTasks])

        sortedUnassignedTasks = sorted(unassignedTasks, key=lambda x:(len(self._availablePeople(x)), x.date, x.type))

        for task in sortedUnassignedTasks:
            person = self.ps.selectFrom(self._availablePeople(task))
            assignedTasks.append(Task(task.date, task.type, person))

        return assignedTasks

print(assignTasks(TASKS))
