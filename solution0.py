# Python Glasgow Dojo
# Solution 0
# Changed for python3.6
from collections import defaultdict, namedtuple
from datetime import date

Task = namedtuple('Task', ['date','type','assignee'])

TASKS = [
        Task( date(2017,10,25), 'driving', None),
        Task( date(2017,10,25), 'shopping', None),
        ]

SKILLS = {
        'driving': set(['Roger','Gordon','Erfan']),
        'cooking': set(['Yuri','Erfan']),
        }

VACATIONS = defaultdict(set, {
    date(2017,10,25) : set(),
    date(2017,10,26) : set(['Roger', 'Gordon']),
    })

"""
A simple solution for the case of:
    * no skills
    * no vacations
    * no volunteers
"""

def assignTasks(tasks):
    import itertools # better to put imports outside functions

    all_people = ['Erfan', 'Gordon', 'Roger', 'Yuri']
    personCycle = itertools.cycle(all_people)
    assignedTasks = []
    for task in tasks:
        selectedPerson = next(personCycle)

        assignedTasks.append(
                Task(task.date, task.type, selectedPerson)
                )
    return assignedTasks

print(assignTasks(TASKS))
