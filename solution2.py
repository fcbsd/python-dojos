# Python Glasgow Dojo
# Solution 2
# Changed for python3.6
from collections import Counter, defaultdict, namedtuple
from datetime import date

Task = namedtuple('Task', ['date','type','assignee'])

TASKS = [
        Task( date(2017,10,25), 'driving', None),
        Task( date(2017,10,25), 'shopping', None),
        Task( date(2017,10,25), 'cooking', None),
        ]

SKILLS = {
        'driving': set(['Roger','Gordon','Erfan']),
        'cooking': set(['Yuri','Erfan']),
        }

VACATIONS = defaultdict(set, {
    date(2017,10,25) : set(),
    date(2017,10,26) : set(['Roger', 'Gordon']),
    })

"""
An object-oriented solution for the case of:
    * no skills
    * no vacations
    * no volunteers

We create a classs called PersonSelector, which selects a 
person from a group of eligible people:
 * The class will will keep track of how many tasks have been
 assigned to each person.
 * The person with the fewest assigned chores will be assigned
 the next task.

Eligible people for a given task *might* not include everyone 
on our road-trip.
"""

def assignTasks(tasks):
    all_people = set(['Erfan', 'Gordon', 'Roger', 'Yuri'])
    personSelector = PersonSelector(all_people)

    eligiblePeople = all_people # TODO: change to add skills
    assignedTasks = []
    for task in tasks:
        selectedPerson = personSelector.selectFrom(eligiblePeople)

        assignedTasks.append(
                Task(task.date, task.type, selectedPerson)
                )
    return assignedTasks

class PersonSelector(object):

    def __init__(self, allPeople):
        self.counter = Counter({x:0 for x in allPeople})

    def selectFrom(self, eligiblePersonSet):
        for person, _cnt in reversed(self.counter.most_common()):
            if person in eligiblePersonSet:
                self.counter[person] += 1
                return person


print(assignTasks(TASKS))
