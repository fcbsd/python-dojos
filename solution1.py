# Python Glasgow Dojo
# Solution 1
# Changed for python3.6
from collections import defaultdict, namedtuple
from datetime import date
import itertools

Task = namedtuple('Task', ['date','type','assignee'])

TASKS = [
        Task( date(2017,10,25), 'driving', None),
        Task( date(2017,10,25), 'shopping', None),
        ]

SKILLS = {
        'driving': set(['Roger','Gordon','Erfan']),
        'cooking': set(['Yuri','Erfan']),
        }

VACATIONS = defaultdict(set, {
    date(2017,10,25) : set(),
    date(2017,10,26) : set(['Roger', 'Gordon']),
    })

"""
A simple solution for the case of:
    * no skills
    * no vacations
    * no volunteers

We create a classs called PersonSelector, which selects a 
person from a group of eligible people. The class will use
itertools.cycle.

Eligible people for a given task *might* not include everyone 
on our road-trip.
"""

def assignTasks(tasks):
    all_people = set(['Erfan', 'Gordon', 'Roger', 'Yuri'])
    personSelector = PersonSelector(all_people)

    eligiblePeople = all_people # TODO: change to add skills
    assignedTasks = []
    for task in tasks:
        selectedPerson = personSelector.selectFrom(eligiblePeople)

        assignedTasks.append(
                Task(task.date, task.type, selectedPerson)
                )
    return assignedTasks

class PersonSelector(object):

    def __init__(self, all_people):
        # itertools.cycle('ABCD') --> A B C D A B C D A B C D ...
        self.personCycle = itertools.cycle(all_people)
        self.all_people = set(all_people) # for sanity check

    def selectFrom(self, eligiblePersonSet):
        # verify we can find an eligible person in the cycle
        if eligiblePersonSet & self.all_people:
            for person in self.personCycle:
                if person in eligiblePersonSet:
                    return person


print(assignTasks(TASKS))
